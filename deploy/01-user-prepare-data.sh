#!/bin/sh

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`
DATADIR="/var/tmp/organisms-data"

# Run tools to acquire organism data.

if [ ! -e "$DATADIR" ] ; then
    mkdir -p "$DATADIR"
fi

"$THISDIR/tools/fetch_taxonomy" "$DATADIR"
