#!/bin/sh

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Install system dependencies.

xargs dnf -y install < "$BASEDIR/requirements-sys.txt"
