Organisms
=========

This software produces data containing organism details from Entrez Taxonomy's
``taxdump`` file that can be incorporated into the JASPAR and UniBind
databases.


Fetching Organism Data
----------------------

Currently, there is only one tool available which fetches and processes the
organism details:

.. code:: shell

  tools/fetch_taxonomy

This tool produces a simple mapping from taxonomy identifiers to scientific
names in a file called ``tax_names.txt``.
