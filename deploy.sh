#!/bin/sh

THISDIR=`dirname "$0"`
DEPLOY="$THISDIR/deploy"

if [ "$USER" = 'root' ] ; then
    cat 1>&2 <<EOF
Only some scripts run by this program need elevated privileges. Run this
command as a suitable unprivileged user and prompting for additional privileges
will occur.
EOF
    exit 1
fi

if [ "$1" = '-n' ] ; then
    RUN=echo
    shift 1
else
    RUN=
fi

for FILENAME in `ls -1 "$DEPLOY" | sort` ; do

    # Run NN-root-* scripts as root.

    if echo "$FILENAME" | grep -q -e '^[[:digit:]]\+-root-' ; then
        if ! $RUN sudo "$DEPLOY/$FILENAME" $* ; then
            exit 1
        fi

    # Run NN-user-* scripts as the current user.

    elif echo "$FILENAME" | grep -q -e '^[[:digit:]]\+-user-' ; then
        if ! $RUN "$DEPLOY/$FILENAME" $* ; then
            exit 1
        fi
    fi
done
